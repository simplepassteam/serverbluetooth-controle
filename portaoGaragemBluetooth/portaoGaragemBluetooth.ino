String message; //string that stores the incoming message
String parar_app = "stop_ble";
long intervalo = 1000; //Variável para armazenar a frequência do BLINK do LED Amarelo

void setup()
{
  Serial.begin(9600); //set baud rate
  pinMode(8, OUTPUT);
}

void loop()
{
  while(Serial.available())
  {//while there is data available on the serial monitor
    message+=char(Serial.read());//store string from serial command
  }
  if(!Serial.available())
  {
    if(message == "open")
    {//if data is available
      Serial.print(message);
      message=""; //clear the data           
      
      digitalWrite(8, HIGH);
      delay(intervalo);          
      digitalWrite(8, LOW);
      
    }
  }
}
